let express = require('express');
let router = express.Router();
let home = require('../controllers/home');
let moment = require('moment');

/* GET home page. */
router.get('/', (req, res, next) => {
  res.json({
    statusCode: 404,
    dateTime: moment(),
    body: req.response
  })
});

module.exports = router;
