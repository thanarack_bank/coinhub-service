const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const moment = require('moment');
const index = require('./routes/index');
const users = require('./routes/users');
const login = require('./routes/login');
//let mongoose = require('mongoose');
//mongoose.connect('mongodb://superAdmin:hEqQF9ahJe@128.199.133.11:27017/isell-prod');

const app = express();

moment.locale('en');

/*app.use(function (req, res, next) {
  var allowedOrigins = ['http://localhost:3006', 'http://localhost:8080'];
  var origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Acc" +
    "ess-Control-Request-Method, Username, Password, Access-Control-Request-Headers");
  next();
});*/

// view engine setup
/*app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');*/

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/login', login);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(200);
  res.json({
    'statusCode': 404,
    'dateTime': moment().format('LLLL')
  })
});

module.exports = app;