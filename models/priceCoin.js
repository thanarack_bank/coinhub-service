const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SchemaData = {
  symbol: String,
  usb: Number,
  eur: Number,
  thb: Number,
  btc: Number,
  createAt: Date
};
const modal = new Schema(SchemaData);
const priceCoin = mongoose.model('pricecoin', modal);
module.exports = priceCoin;
