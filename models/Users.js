const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SchemaData = {
    name: String,
    lastname: String,
    username: String,
    password: String,
    userActive: String,
    createAt: Date,
    updateAt: Date
};
const UsersmModal = new Schema(SchemaData);
const UsersTable = mongoose.model('users', UsersmModal);
module.exports = UsersTable;