const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SchemaData = {
  symbol: String,
  high: Number,
  low: Number,
  open: Number,
  volumefrom: Number,
  volumeto: Number,
  close: Number,
  typehistory: String,
  timeUnix: String,
  time: Date,
  currency: String,
  createAt: Date
};
const modal = new Schema(SchemaData);
const historyCoin = mongoose.model('historyCoin', modal);
module.exports = historyCoin;
