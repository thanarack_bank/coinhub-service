const mongoose = require('mongoose');
mongoose.connect('mongodb://coinhub_admin:B2kPi4ZZU1xitVqO@128.199.133.11:27017/coinhub');

const Schema = mongoose.Schema;
const SchemaData = {
  idSync: Number,
  symbol: String,
  coinName: String,
  fullName: String,
  name: String,
  totalCoinSupply: Number,
  algorithm: String,
  endpoint: String,
  sortOrder: Number,
  createAt: Date,
  updateAt: Date
};
const modal = new Schema(SchemaData);
const coinTable = mongoose.model('coins', modal);
module.exports = coinTable;