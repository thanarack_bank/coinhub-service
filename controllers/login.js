const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const UsersTable = require('../models/Users');

const headerLoginCheck = (req, res, next) => {
  req.loginForm = {
    username: req.headers.username,
    host: req.header.host
  };
  if (!req.headers.username) {
    res.status(200).json({'statusCode': 404, 'message': 'Header username not found'});
  }
  if (!req.headers.password) {
    res.status(200).json({'statusCode': 404, 'message': 'Header password not found'});
  }
  return next();
}

const generateTokenUser = (req, res, next) => {
  UsersTable.find({
    username: req.loginForm.username.toLowerCase()
  }, async (err, data) => {
    if (err) {
      return res.status(500).json({status: 500, error: err});
    }
    if (data.length === 0) {
      return res.status(200).json({'statusCode': 404, 'message': 'User not found'});
    } else {
      if (data[0].userActive === 'cancle') {
        return res.status(200).json({'statusCode': 404, 'message': 'User has been ban'});
      }
      const checkPasswordUser = await checkPassword(req.headers.password, data[0].password);
      if (!checkPasswordUser) {
        return res.status(200).json({'statusCode': 404, 'message': 'Password wrong'});
      } else {
        req._token = await generateToken(req.loginForm);
        return next();
      }
    }
  });
}

const checkPassword = async (password, hashPassword) => {
  return new Promise((resolve) => {
    bcrypt.compare(password, hashPassword, function (err, res) {
      resolve(res);
    });
  })
}

const genPasswordHash = (password) => {
  return new Promise((resolve) => {
    bcrypt.hash(password, 10, function (err, hash) {
      resolve(hash);
    });
  });
}

const generateToken = (data) => {
  return new Promise((resolve) => {
    jwt.sign(data, 'shhhhh', {
      algorithm: 'HS512'
    }, (err, token) => {
      resolve(token);
    });
  });
}

const createUser = (req, res, next) => {
  UsersTable.find({
    username: req.headers.username
  }, async (err, data) => {
    if (err) {
      return res.status(500).json({status: 500, error: err});
    }
    if (data.length !== 0) {
      return res.status(200).json({'statusCode': 404, 'message': 'User exits'});
    } else {
      const dataSaveDb = {
        name: req.body.name.toLowerCase(),
        lastname: req.body.lastname.toLowerCase(),
        userActive: 'active',
        username: req.headers.username.toLowerCase(),
        password: await genPasswordHash(req.headers.password),
        createAt: new Date(),
        updateAt: new Date()
      }
      const createUser = new UsersTable(dataSaveDb);
      createUser.save();
      return next();
    }
  });
}

module.exports = {
  headerLoginCheck,
  generateTokenUser,
  createUser
}