const request = require('request');
const tableCoin = require('../models/tableCoin');
const priceCoin = require('../models/priceCoin');
const historyCoin = require('../models/historyCoin');
const moment = require('moment');
require('../server/connectMongo');

const saveCoin = (data) => {
  const coinObject = data.Data;
  for (let key in coinObject) {
    if (coinObject.hasOwnProperty(key)) {
      const obj = coinObject[key];
      tableCoin.find({idSync: obj.Id}, (err, result) => {
        if (result.length === 0) {
          creteData(obj);
        } else {
          updateData(obj);
        }
      })
    }
  }
};

const updateData = (obj) => {
  const date = new Date();
  const setObjDb = {
    idSync: obj.Id,
    symbol: obj.Symbol,
    coinName: obj.CoinName,
    fullName: obj.FullName,
    name: obj.Name,
    totalCoinSupply: obj.TotalCoinSupply,
    algorithm: obj.Algorithm,
    sortOrder: obj.SortOrder,
    updateAt: date
  }
  tableCoin.updateOne({
    idSync: obj.Id
  }, setObjDb, {}, (err, res) => {
    //
  });
}

const creteData = (obj) => {
  const date = new Date();
  const setObjDb = {
    idSync: obj.Id,
    symbol: obj.Symbol,
    coinName: obj.CoinName,
    fullName: obj.FullName,
    name: obj.Name,
    totalCoinSupply: obj.TotalCoinSupply,
    algorithm: obj.Algorithm,
    endpoint: 'cryptocompare.com',
    sortOrder: obj.SortOrder,
    createAt: date,
    updateAt: date
  }
  tableCoin.create(setObjDb, (err) => {
    //
  });
}

const checkPriceCoin = (data) => {
  let i = 0;
  const totalCoin = (data.length) - 1;
  const myLoop = () => {
    setTimeout(async () => {
      if (i <= totalCoin) {
        await getData(i);
        myLoop();
      }
      i++;
    }, 0);
  };
  const getData = (key) => {
    const dataCoin = data[key];
    const symbol = dataCoin.symbol;
    const options = {
      method: 'GET',
      url: 'https://min-api.cryptocompare.com/data/price?fsym=' + symbol + '&tsyms=BTC,USD,EUR,THB',
      headers: {
        'User-Agent': 'request'
      }
    };
    return new Promise(resolve => {
      request(options, async (err, response, body) => {
        if (response && response.statusCode === 200) {
          const dataJson = JSON.parse(body);
          let resultUpdate = [];
          if (dataJson.Response !== 'Error') {
            resultUpdate = await updatePriceCoin(symbol, dataJson);
          }
          resolve(resultUpdate);
        }
      });
    })
  };
  myLoop();
};

const updatePriceCoin = (symbol, data) => {
  const date = new Date();
  const setObjDb = {
    symbol: symbol,
    usb: data.USD,
    eur: data.EUR,
    thb: data.THB,
    btc: data.BTC,
    createAt: date
  };
  // priceCoinTable.create(setObjDb, (err, result) => {
  //   return new Promise(resolve => {
  //     resolve(result);
  //   });
  // });
  priceCoin.updateOne({symbol: symbol}, setObjDb, {upsert: true}, (err, result) => {
    console.log(result);
    return new Promise(resolve => {
      resolve(result);
    })
  });
};

const checkCoinHistory = (data, allData = false) => {
  let i = 0;
  let allDataSring = 'false';
  if (allData) allDataSring = 'true';
  const totalCoin = (data.length) - 1;
  const myLoop = () => {
    setTimeout(async () => {
      if (i <= totalCoin) {
        console.log(i);
        await getData(i);
        myLoop();
      }
      i++;
    }, 0);
  };
  const getData = (key) => {
    const dataCoin = data[key];
    const symbol = dataCoin.symbol;
    const options = {
      method: 'GET',
      url: 'https://min-api.cryptocompare.com/data/histoday?fsym=' + symbol + '&tsym=USD&aggregate=1&e=CCCAGG&allData=' + allDataSring,
      headers: {
        'User-Agent': 'request'
      }
    };
    return new Promise(resolve => {
      request(options, async (err, response, body) => {
        if (response && response.statusCode === 200) {
          const dataJson = JSON.parse(body);
          let resultUpdate = [];
          if (dataJson.Response === 'Success') {
            resultUpdate = await updateHistoryCoinDay(symbol, dataJson);
          }
          resolve(resultUpdate);
        }
      });
    })
  };
  myLoop();
};

const updateHistoryCoinDay = async (symbol, data) => {
  const listDate = data.Data;
  await Promise.all(listDate.map(async (val) => {
    const setObjDb = {
      symbol: symbol,
      high: val.high,
      low: val.low,
      open: val.open,
      volumefrom: val.volumefrom,
      volumeto: val.volumeto,
      close: val.close,
      typehistory: 'day',
      timeUnix: val.time,
      time: moment.unix(val.time).format(),
      currency: 'usd',
      createAt: new Date()
    };
    // historyCoin.create(setObjDb, (err, result) => {
    //   //
    // });
    await historyCoin.updateOne({timeUnix: val.time, symbol: symbol}, setObjDb, {upsert: true}, (err, result) => {
      //
    });
  }));
};

exports.coinlist = () => {
  const options = {
    method: 'GET',
    url: 'https://min-api.cryptocompare.com/data/all/coinlist',
    headers: {
      'User-Agent': 'request'
    }
  };
  request(options, (err, response, body) => {
    const dataJson = JSON.parse(body);
    return saveCoin(dataJson);
  });
}

exports.updatePrice = () => {
  tableCoin.find({active: 1}).sort({sortOrder: 1}).exec((err, result) => {
    checkPriceCoin(result);
  });
};

exports.updateHistoryCoin = () => {
  tableCoin.find({active: 1}).sort({sortOrder: 1}).exec((err, result) => {
    checkCoinHistory(result, true);
  });
};