const request = require('request');

exports.homecon = (req, res, next) => {
  req.varseven = '1234';
  next();
};

exports.homecon2 = (num) => (req, res, next) => {
  req.number = num;
  next();
};

exports.getCoinList = (req, res, next) => {
  request({
    method: 'GET',
    url: 'https://min-api.cryptocompare.com/data/all/coinlist'
  }, (err, response, body) => {
    const json = JSON.parse(body)
    req.response = json.Data
    return next();
  })
}
